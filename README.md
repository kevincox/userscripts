# kevincox's User Scripts

Install them at https://userscripts.kevincox.ca

Source is at https://gitlab.com/kevincox/userscripts

[Learn about user scripts here](https://greasyfork.org)

## Developing

```console
$ # Get dependencies.
$ npm install
$ # Start webserver.
$ npx parcel --no-cache *.user.ts
$ # Build.
$ npx parcel build --no-cache *.user.ts
```
